# Converge Backend Test

## Coding challenge

If you’re reading this, that means that Converge is really interested in your engineering abilities. Congratulations!

This is intended to be a small coding exercise meant to gauge your programming style. Now’s your chance to show off your design skills (simple but extensible), your testing habits (hey hey unit-tests), and your keen attention to detail.

Included is an input file, `data.csv`, that you will write a Python program to process. Your solution should be documented and tested to production standards. It should follow good Python conventions, and should be easy to get running.

## Submission

Fork this repo and regularly commit your changes like you would if you were coding on the job. When forking the repo, make sure it's private, and include us as members. Once your code is ready, send it to us. It's that easy! Please include

* A `README.md` file with instructions on how to run your program
* The result of processing the input file.

## The problem

You’re tasked with taking entries of personal information in multiple formats and normalizing each entry into a standard JSON format. Write your formatted, valid JSON out to a file with two-space indentation and keys sorted alphabetically.

### Input

Your program will be fed an input file of n lines. We have provided the sample, `data.txt`. Each line contains `entry` information, which consists of a first name, last name, phone number, color, and zip code.

The order and format of these lines vary in three separate ways.  The three different formats are as follows:

```csv
Lastname, Firstname, (703)-742-0996, Blue, 10013
Firstname Lastname, Red, 11237, 703 955 0373
Firstname, Lastname, 10013, 646 111 0101, Green
```

Some lines may be invalid and should not interfere with the processing of subsequent valid lines.  A line should be considered invalid if its phone number / zipcode does not contain the proper number of digits. You can assume zipcodes are strings of 5 digits.

### Output

The program should write a valid, formatted JSON object out to `result.json`. The JSON representation should be indented with two spaces and the keys should be sorted in ascending order.

Successfully processed lines should result in a normalized addition to the list associated with the `entries` key. Phone numbers should be normalized using the format of xxx-xxx-xxxx. For lines that were unable to be processed, a line number i (where 0 ≤ i < n) for each faulty line should be appended to the list associated with the `errors` key.

The `entries` list should be sorted in ascending alphabetical order by (last name, first name).

## Sample

For the input

```csv
Booker T., Washington, 87360, 373 781 7380, yellow
Chandler, Kerri, (623)-668-9293, pink, 123123121
James Murphy, yellow, 83880, 018 154 6474
asdfawefawea
```

We should receive the output

```json
{
  "entries": [
    {
      "color": "yellow",
      "firstname": "James",
      "lastname": "Murphy",
      "phonenumber": "018-154-6474",
      "zipcode": "83880"
    },
    {
      "color": "yellow",
      "firstname": "Booker T.",
      "lastname": "Washington",
      "phonenumber": "373-781-7380",
      "zipcode": "87360"
    }
  ],
  "errors": [
    1,
    3
  ]
}
```

Questions?

Feel free to email.

Good luck!
Your friends at Converge

<img src="https://media-exp1.licdn.com/dms/image/C4D1BAQFQRJMSEXR-PQ/company-background_10000/0/1648847020596?e=1668801600&v=beta&t=1lPdum1ys5CeHvOkodjO_q1HKiWa0jd06GcM_Hc75s0">
